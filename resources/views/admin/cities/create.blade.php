@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add new book</div>

                <div class="card-body">
                <form action="{{ route('admin.cities.store') }}" method="POST">
                    @csrf

                    Name:
                    <input type="text" name="name" class="form-control">
                    <br><br>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
