@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Book</div>

                <div class="card-body">
                <form action="{{ route('admin.cities.update', $city->id) }}" method="POST">
                    {{ method_field('PUT') }}
                    @csrf

                    Name:
                <input type="text" name="name" class="form-control" value="{{ $city->name }}">
                    <br><br>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
