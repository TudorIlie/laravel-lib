@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Your Booklist</div>

                <div class="card-body">

                <a href="{{ route('admin.cities.create') }}" class="btn btn-sm btn-primary">Add new book</a>
                    <br><br>
                   <table class="table">
                       <tr>
                           <th>Book name</th>
                           <th></th>
                       </tr>
                       @forelse ($cities as $city)
                            <tr>
                                <td>
                                    {{$city->name}}
                                </td>
                            <td>
                                <a href="{{ route('admin.cities.edit', $city->id) }}" class="btn btn-sm btn-info">Edit</a>
                            <form action="{{ route('admin.cities.destroy', $city->id) }}" method="POST">
                                @csrf
                                {{ method_field('DELETE') }}
                                <br>
                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-small btn-danger">Delete</button>
                            </form>
                            </td>

                            </tr>
                           
                       @empty
                       <tr>
                           <td colspan="2">No records found</td>
                       </tr>
                           
                       @endforelse
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
